Test for Xpore
===============

The data for the test has been referenced from:
    https://zenodo.org/records/5162402

To download the test data and generate tarball:
   run `./get-test-data`

To run the test, run the test from the project root:
   run `sh ./debian/tests/run-data-preprocess-test`